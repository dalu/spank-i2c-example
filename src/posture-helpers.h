/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __POSTURE_HELPERS__H
#define __POSTURE_HELPERS__H

#include <stdbool.h>
#include <spank/extio/types.h>

bool posture_scan_position(char *str, spank_extio_position_t *v);
bool posture_scan_velocity(char *str, spank_extio_velocity_t *v);
bool posture_scan_stddev_xyz(char *str, spank_extio_stddev_xyz_t *v);
bool posture_dump(spank_extio_posture_t *posture, char *bufptr, size_t buflen);

#define POSTURE_DUMP_MAXLEN						\
    sizeof("<-16384,-16384,-16384>(-16384,-16384,-16384)"		\
	   " "								\
	   "<-16384,-16384,-16384>(-16384,-16384,-16384)")

#endif
